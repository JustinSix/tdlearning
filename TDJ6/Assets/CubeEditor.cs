﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
[SelectionBase]
public class CubeEditor : MonoBehaviour
{
    [Range(1f,20f)] [SerializeField] float gridSize = 10f;

    TextMesh textMesh;

    void Start()
    {
        textMesh = GetComponentInChildren<TextMesh>();
        textMesh.text = "TEST";
    }

    void Update()
    {
   
        Vector3 snapPos;
        //snap position of cube
        snapPos.x = Mathf.RoundToInt(transform.position.x / gridSize) * gridSize;

        snapPos.z = Mathf.RoundToInt(transform.position.z / gridSize) * gridSize;

        transform.position = new Vector3(snapPos.x, 0f, snapPos.z);

        //set text on box
        textMesh = GetComponentInChildren<TextMesh>();
        string labelText = snapPos.x / gridSize + "," + snapPos.z / gridSize;
        textMesh.text = labelText;
        gameObject.name = labelText;

    }

}
